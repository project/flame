<?php
/**
 * @file
 * Contains the theme's functions to manipulate Drupal's default markup.
 */

/**
 * Preprocesses variables for page.tpl.php.
 */
function flame_preprocess_html(&$variables) {
  _flame_load_bootstrap();
}

/**
 * Implements THEME_preprocess_node().
 */
function flame_preprocess_node(&$variables) {
  $variables['user_picture'] = FALSE;
  $variables['submitted'] = FALSE;
  $variables['content']['field_tags']['#title'] = FALSE;
  $variables['content']['links']['comment'] = FALSE;

  $node = $variables['node'];

  $variables['date_day'] = format_date($node->created, 'custom', 'j');
  $variables['date_month'] = format_date($node->created, 'custom', 'F');
  $variables['date_year'] = format_date($node->created, 'custom', 'Y');

  $variables['content']['field_tags']['#theme'] = 'links';
  $variables['content']['field_image'][0]['#attributes']['class'][] = 'img-polaroid';
  // Let's get that read more link out of the generated links variable!
  unset($variables['content']['links']['node']['#links']['node-readmore']);

  // Now let's put it back as it's own variable! So it's actually versatile!
  $variables['newreadmore'] = '<footer>' . l(t('Read More'), 'node/' . $node->nid,
    array('attributes' => array('class' => array('btn btn-mini')))
  ) . '</footer>';
}

/**
 * Add custom class to image styles.
 */
function flame_image_style($variables) {

  // Determine the dimensions of the styled image.
  $dimensions = array(
    'width' => $variables['width'],
    'height' => $variables['height'],
  );

  image_style_transform_dimensions($variables['style_name'], $dimensions);

  $variables['width'] = $dimensions['width'];
  $variables['height'] = $dimensions['height'];

  // Determine the URL for the styled image.
  $variables['path'] = image_style_url($variables['style_name'], $variables['path']);

  // Begin custom snippet.
  // Add or append custom classes, to avoid clobbering existing.
  if (isset($variables['attributes']['class'])) {
    $variables['attributes']['class'] += array('img-polaroid', $variables['style_name']);
  }
  else {
    $variables['attributes']['class'] = array('img-polaroid', $variables['style_name']);
  }

  /* End custom snippet */
  return theme('image', $variables);
}

/**
 * Implements THEME_form_alter().
 */
function flame_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'search_block_form') {
    $form['#attributes']['class'][] = 'form-search';
    $form['search_block_form']['#attributes']['class'][] = 'span3 search-query';
    $form['actions']['submit']['#attributes']['class'][] = 'btn';
  }

  if ($form_id == 'search_form') {
    $form['basic']['submit']['#attributes']['class'][] = 'btn';
    $form['advanced']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('value for advanced search button'),
      '#prefix' => '<div class="action">',
      '#suffix' => '</div>',
      '#weight' => 100,
      '#attributes' => array('class' => array('btn')),
    );
  }
}

/**
 * Implements theme_breadcrumb().
 */
function flame_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];
  $spr = ' &gt; ';
  $delimiter = check_plain(theme_get_setting('breadcrumb_delimiter'));
  // Use CSS to hide titile .element-invisible.
  $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';
  // Comment below line to hide current page to breadcrumb.
  $breadcrumb[] = drupal_get_title();

  if (!empty($delimiter)) {
    $output .= '<nav class="breadcrumb">' . implode($delimiter, $breadcrumb) . '</nav>';
    return $output;
  }
  else {
    $output .= '<nav class="breadcrumb crumb">' . implode($spr, $breadcrumb) . '</nav>';
    return $output;
  }
}

/**
 * Loads Twitter Bootstrap library.
 */
function _flame_load_bootstrap() {
  $version = check_plain(theme_get_setting('bootstrap_version'));
  $js_path = '/js/bootstrap.min.js';
  $js_options = array(
    'group' => JS_LIBRARY,
  );
  $css_path = '/css/bootstrap.min.css';
  $cssr_path = '/css/bootstrap-responsive.min.css';
  $css_options = array(
    'group' => CSS_THEME,
    'weight' => -1000,
    'every_page' => TRUE,
  );
  switch (theme_get_setting('bootstrap_source')) {
    case 'bootstrapcdn':
      $bootstrap_path = '//netdna.bootstrapcdn.com/twitter-bootstrap/' . $version;
      $js_options['type'] = 'external';
      $css_path = '/css/bootstrap-combined.min.css';
      unset($cssr_path);
      $css_options['type'] = 'external';
      break;

    case 'libraries':
      if (module_exists('libraries')) {
        $bootstrap_path = libraries_get_path('bootstrap');
      }
      break;

    case 'theme';
      $bootstrap_path = path_to_theme() . '/libraries/bootstrap';
      break;

    default:
      return;
  }
  _flame_add_asset('js', $bootstrap_path . $js_path, $js_options);
  _flame_add_asset('css', $bootstrap_path . $css_path, $css_options);
  if (isset($cssr_path)) {
    _flame_add_asset('css', $bootstrap_path . $cssr_path, $css_options);
  }
}

/**
 * Adds js/css file.
 */
function _flame_add_asset($type, $data, $options) {
  if (isset($options['browsers']) && !is_array($options['browsers'])) {
    $options['browsers'] = _flame_browsers_to_array($options['browsers']);
  }
  switch ($type) {
    case 'css':
      drupal_add_css($data, $options);
      break;

    case 'js':
      if (isset($options['browsers'])) {
        $data = file_create_url($data);
        $elements = array(
          '#markup' => '<script type="text/javascript" src="' . $data . '"></script>',
          '#browsers' => $options['browsers'],
        );
        $elements = drupal_pre_render_conditional_comments($elements);
        _flame_add_html_head_bottom(drupal_render($elements));
      }
      else {
        drupal_add_js($data, $options);
      }
      break;

  }
}
